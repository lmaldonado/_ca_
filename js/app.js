var app = angular.module('CA', ['ui.swiper','ngAnimate']);

var swiperCtrl;

app.controller('configCtrlCH', ['$scope', '$http', '$interval', '$timeout' ,'$location' , '$rootScope' , '$sce' , function ($scope, $http, $interval, $timeout,$location,$rootScope,$sce) {
	//CONFIGURACION
	url = "http://localhost:8080/cartelera_backend_laravel/public/";
	url2 = "http://localhost:8080/cartelera_backend_laravel/public/";
	
	/***********************verificar la sala ****************************/
	    var name='sala';
	 	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
	    var  idsala= regex.exec($location.absUrl());
    /***********************fin verificar la sala ************************/
    /***********************verificar la sala ****************************/
	    var name='fp';
	 	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
	    var  fp= regex.exec($location.absUrl());
    /***********************fin verificar la sala ************************/
    $http.get(url+'?ruta=api/iniciasesion&finger='+fp).then(function(data){
    	$scope.sesion=data.data;
		
		
	})	
	$scope.license_message = '';
	$scope.license_alert = false;

	$rootScope.ip = '';
	console.log('HOST:',$location.host());
	if($location.host() != 'localhost'){
		$scope.license_alert = true;
	}
	$scope.vista_previa=function(){
		$scope.license_alert = false;

		setTimeout(function(){
			$scope.license_alert = true;
		}, 5000);
	}

	$http.get(url+'?ruta=api/licencia_info').then(function(data){
		console.log('LICENCIAS',data.data);
		var licencias = data.data;
		$scope.cont=0;
		for(var i=0;i<licencias.length;i++){
			
			if(licencias[i].product_id==10 && licencias[i].status=='Active' && licencias[i].descarga==1){
				
				$scope.cont++;
			}		
		}
		if($scope.cont==0){
			url = encodeURI('http://localhost:8080/cartelera_backend_laravel/public/?ruta=config/licencia');
			window.location.replace(url);
		}
	})
	$interval(function(){
		$http.get(url+'?ruta=api/licencia_info').then(function(data){
			console.log('LICENCIAS',data.data);
			var licencias = data.data.licencia;
			$scope.cont=0;
			for(var i=0;i<licencias.length;i++){
				
				if(licencias[i].product_id==10 && licencias[i].status=='Active' && licencias[i].descarga==1){
					
					$scope.cont++;
				}		
			}
			if($scope.cont==0){
				url = encodeURI('http://localhost:8080/cartelera_backend_laravel/public/?ruta=config/licencia');
				window.location.replace(url);
			}
		})
	},3000);

	$interval(function(){
		$http.get(url+'?ruta=api/config/ch').then(function(data){
			$scope.configCH = data.data.configCH;
			if($scope.configCH.tipo_servidor == 'web'){
				
				$http.get(url+'?ruta=config/ch/sync/servicios&url='+$scope.configCH.servidor_web).then(function(data){
					console.log('SINCRONIZACION2');
				},function(err){
					console.log('ERROR DE SINCRONIZACION2');
				});
			}		
		});
	},120000);

	
	// $scope.sala_id = idsala[1];
	// $rootScope.id_sala = idsala[1];
	$rootScope.configcs={};
	$scope.url = url;
	$scope.url2 = url2;
	$scope.config = {};
	$scope.servicios = {};
	$scope.homenajes = {};
	// $rootScope.showPublis = true;
	$rootScope.duracion_slider_homenaje = 5000;
	$rootScope.homenaje=false;
	$rootScope.hide_servicio=false;
	$rootScope.hide_publicidad=true;
	
	$scope.fecha = new Date();

	// $scope.onInit = function (swiper) {
 //    	$scope.swiper = swiper;
 //    	$scope.swiper2 = swiper;
	// };
	//HOMENAJES
	// CONFIGURACION

	$http.get(url+'?ruta=api/config/ch').then(function(data){
			
			console.log('linea 27',data.data);
			$scope.configCH = data.data.configCH;
			// $rootScope.ip = data.data.ip;
						
			$rootScope.duracion_slider_homenaje = data.data.configCH.duracion_slider_homenaje * 1000;
			$rootScope.duracion_transicion_slider_homenaje = data.data.configCH.duracion_transicion_slider_homenaje * 1000;
			
			// $scope.swiper.params.speed = $scope.duracion_transicion_slider_homenaje;
			// $scope.swiper.params.autoplay = $scope.duracion_slider_homenaje;
			

			//PUBLICIDADES
			$rootScope.publis 		  = data.data.publis;
			$rootScope.maxIndexPublis = (data.data.publis.length > 0)?data.data.publis.length-1:0;

		});
	$http.get(url+'?ruta=api/config/ni').then(function(res){
		$rootScope.configNI = res.data.configNI;

		$scope.logo_encabezado = url2 + 'images/empresa/'+$rootScope.configNI.logo_encabezado;
		$scope.url_logo_pie = url2 + 'images/empresa/'+$rootScope.configNI.logo_pie;
		// var color =$scope.configNI.color_back;
		if($scope.configNI.color_back.substring(0,1) == '#') {
		 	var color = $scope.configNI.color_back.substring(1);
		}
		var r = parseInt(color.substring(0,2),16);
		var g = parseInt(color.substring(2,4),16);
		var b = parseInt(color.substring(4),16);

		$scope.color_fondo ="rgba("+r+","+g+","+b+",0.6)";
		// $scope.configNI.color_cont = chroma(color).darken().hex();
		
	});	
	
	$scope.configCA = function(){
		$http.get(url+'?ruta=api/config/ch').then(function(data){
			
			console.log('linea 27',data.data);
			$scope.configCH = data.data.configCH;
			$rootScope.configcs = data.data.configCH;
			// $rootScope.ip = data.data.ip;
			// console.log('IP: '+$rootScope.ip);
			
			$scope.musica_funcional = data.data.configCH.musica_funcional;
			$scope.cartelera_informacion = data.data.configCH.cartelera_informacion	;
			$scope.cartelera_control = data.data.configCH.cartelera_control;
			$rootScope.duracion_slider_homenaje = data.data.configCH.duracion_slider_homenaje * 1000;
			$rootScope.duracion_transicion_slider_homenaje = data.data.configCH.duracion_transicion_slider_homenaje * 1000;
			
			// $scope.swiper.params.speed = $scope.duracion_transicion_slider_homenaje;
			// $scope.swiper.params.autoplay = $scope.duracion_slider_homenaje;
			

			// //PUBLICIDADES
			// $rootScope.publis 		  = data.data.publis;
			// $rootScope.maxIndexPublis = (data.data.publis.length > 0)?data.data.publis.length-1:0;

			$rootScope.intervalo_publicidadOriginal = data.data.configCH.intervalo_publicidad * (60 * 1000); //TIEMPO INICIAL PARA MOSTRAR LA PRIMERA PUBLICIDAD EN MINUTOS
			$rootScope.duracion_publicidadOriginal 	= data.data.configCH.duracion_publicidad * 1000; //TIEMPO QUE SE MUESTRA UNA PUBLICIDAD EN SEGUNDOS
			$rootScope.intervalo_publicidadOriginal_new = $rootScope.intervalo_publicidadOriginal + $rootScope.duracion_publicidadOriginal;
			
			// if ( $scope.configCH.tipo_encabezado == "texto" ) {
			// 	$scope.texto_encabezado = $scope.configCH.texto_encabezado;
			// 	$scope.logo_encabezado = "";
			// } else {
			// 	$scope.texto_encabezado = "";
				// $scope.logo_encabezado = url2 + 'images/empresa/logo.png';
			// }
			// $scope.url_logo_pie = url + 'images/ch/logos/' + $scope.configCH.logo_pie;
			// $scope.url_logo_pie = url2 + 'images/empresa/logo_pie.png';
			// $scope.qrcode = url + 'images/ch/qrcodes/' + $scope.configCH.qrcode;
			// $scope.functHomenajes();
		});
		$http.get(url+'?ruta=api/config/ni').then(function(res){
			$rootScope.configNI = res.data.configNI;

			$scope.logo_encabezado = url2 + 'images/empresa/'+$rootScope.configNI.logo_encabezado;
			$scope.url_logo_pie = url2 + 'images/empresa/'+$rootScope.configNI.logo_pie;
			// var color =$scope.configNI.color_back;
			if($scope.configNI.color_back.substring(0,1) == '#') {
			 	var color = $scope.configNI.color_back.substring(1);
			}
			var r = parseInt(color.substring(0,2),16);
			var g = parseInt(color.substring(2,4),16);
			var b = parseInt(color.substring(4),16);

			$scope.color_fondo ="rgba("+r+","+g+","+b+",0.6)";
			// $scope.configNI.color_cont = chroma(color).darken().hex();
			
		});	
	}
	$scope.configCA();	
	
	$scope.gets = function() {
		

		$scope.CurrentDate = new Date();
		$scope.Hora = ($scope.CurrentDate.getHours()<10?"0":"")+$scope.CurrentDate.getHours()+':'+ ($scope.CurrentDate.getMinutes()<10?'0':'') + $scope.CurrentDate.getMinutes()+':'+ ($scope.CurrentDate.getSeconds()<10?'0':'') + $scope.CurrentDate.getSeconds();
		$scope.FechaHoy = $scope.CurrentDate.getFullYear()+"-"+($scope.CurrentDate.getMonth()+1)+"-"+(($scope.CurrentDate.getDate())<10?'0':'')+($scope.CurrentDate.getDate());
		$scope.FechaManana = $scope.CurrentDate.getFullYear()+"-"+($scope.CurrentDate.getMonth()+1)+"-"+(($scope.CurrentDate.getDate()+1)<10?'0':'')+($scope.CurrentDate.getDate()+1);

		$http.get(url+'?ruta=api/config/ch').then(function(data2){
			$scope.configCH = data2.data.configCH;
			var urlServ='';
			if($scope.configCH.tipo_servidor == 'online'){
				urlServ = $scope.configCH.servidor_url+'cartelera_backend_laravel/public/';
				
			}else{
				if($scope.configCH.tipo_servidor == 'local'){
					urlServ = 'http://'+$scope.configCH.servidor_ip+'/cartelera_backend_laravel/public/';
				}else{
					urlServ = 'http://localhost:8080/cartelera_backend_laravel/public/';
				}
			}

			if(idsala && idsala[1]>0){
				console.log('ES UN DEMO');
				$scope.id_sala = idsala[1];
			}else{
				$scope.id_sala = $scope.configCH.sala_id;
			}

			//SERVICIO
			var consulta=$http.get(urlServ+'?ruta=api/servicio&id=' + $scope.id_sala).then(function(data) {
				

				$scope.servicios = data.data;				

				console.log('linea 80',data.data);
				if(data.data.id){

					if($scope.servicios.cortejo_act == 1){
						$scope.trayectoria = $sce.trustAsResourceUrl($scope.servicios.cortejo_link);
					}				

					if($rootScope.configNI.tipo_qrcode == 'QR1'){
						$scope.qrcode = urlServ + 'images/servicios/qrcodes/' + $scope.servicios.qrcode1;
						
						var str = $scope.servicios.url1; 
	    				var http = str.slice(0, 7);
	    				var www = str.slice(7, 11);
	    				var rest = str.slice(11);
	    				var rest2 = str.slice(7);
	    				var https = str.slice(0, 8);
	    				var www2 = str.slice(8, 12);
	    				var rest3 = str.slice(12);
	    				var rest4 = str.slice(8);
	    				var www3 = str.slice(0,4);
	    				var rest5 = str.slice(4);

	    				if(http == 'http://'){
	    					if(www == 'www.'){
	    						$rootScope.url_web_homenaje = www+rest;
	    					}else{
	    						$rootScope.url_web_homenaje = 'www.'+rest2;
	    					}
	    				}else if(https == 'https://'){
	    					if(www2 == 'www.'){
	    						$rootScope.url_web_homenaje = www2+rest3;
	    					}else{
	    						$rootScope.url_web_homenaje = 'www.'+rest4;
	    					}
	    				}else if(www3 == 'www.'){
	    					$rootScope.url_web_homenaje = str;
	    				}else{
	    					$rootScope.url_web_homenaje = $scope.servicios.url1;
	    				}

						
					}else{
						if($rootScope.configNI.tipo_qrcode == 'QR2'){
							$scope.qrcode = url + 'images/ni/qrcodes/' + $rootScope.configNI.qrcode;

							var str = $rootScope.configNI.url_web; 
		    				var http = str.slice(0, 7);
		    				var www = str.slice(7, 11);
		    				var rest = str.slice(11);
		    				var rest2 = str.slice(7);
		    				var https = str.slice(0, 8);
		    				var www2 = str.slice(8, 12);
		    				var rest3 = str.slice(12);
		    				var rest4 = str.slice(8);
		    				var www3 = str.slice(0,4);
		    				var rest5 = str.slice(4);

		    				if(http == 'http://'){
		    					if(www == 'www.'){
		    						$rootScope.url_web_homenaje = www+rest;
		    					}else{
		    						$rootScope.url_web_homenaje = 'www.'+rest2;
		    					}
		    				}else if(https == 'https://'){
		    					if(www2 == 'www.'){
		    						$rootScope.url_web_homenaje = www2+rest3;
		    					}else{
		    						$rootScope.url_web_homenaje = 'www.'+rest4;
		    					}
		    				}else if(www3 == 'www.'){
		    					$rootScope.url_web_homenaje = str;
		    				}else{
		    					$rootScope.url_web_homenaje = $rootScope.configNI.url_web;
		    				}

							// 
						}
					}
					if($scope.servicios.responso_id != 0){
						var fecha = $scope.servicios.responso.split('-');
						fechaResponso = new Date($scope.servicios.responso);
						$scope.servicios.responso_fecha = 'día '+fechaResponso.getDate()+' a las '+fechaResponso.getHours()+':'+(fechaResponso.getMinutes()<10?'0'+fechaResponso.getMinutes():fechaResponso.getMinutes())+' hs.';
						console.log('RESPONSO '+$scope.servicios.responso_fecha);
					}
					// console.log(data.data.nombres);
					if (data.data.nombres!='undefined'){
						
						$rootScope.hide_servicio=true;
						$rootScope.hide_publicidad=false;
						$rootScope.homenaje=true;
						
					}
					fechaCementerio = new Date($scope.servicios.servicio_cementerio);
						$scope.servicios.fechaCementerio = "día "+fechaCementerio.getDate()+" a las "+fechaCementerio.getHours()+':'+(fechaCementerio.getMinutes()<10?'0':'') + fechaCementerio.getMinutes()+" hs.";

					fechaSalida = new Date($scope.servicios.salida);
					$scope.servicios.fechaSalida = "día "+fechaSalida.getDate()+" a las "+fechaSalida.getHours()+':'+(fechaSalida.getMinutes()<10?'0':'') + fechaSalida.getMinutes()+" hs.";

					$scope.servicios.destino.mapa = urlServ + "images/destinos/" + $scope.servicios.destino.mapa;

					if ( $scope.servicios.velatorios.length > 0 ) {

						var existenVelatorios = false;
						angular.forEach($scope.servicios.velatorios, function(value, key){
							fecha = new Date(value.fecha);
							hora = new Date(value.fecha + ' ' + value.desde);
							$scope.fechaVelatorio = fecha.getFullYear()+"-"+(fecha.getMonth()+1)+"-"+((fecha.getDate()+1)<10?'0':'')+(fecha.getDate()+1);
							$scope.fechaCortaVelatorio = fecha.getDate()+'-'+(fecha.getMonth()+1);
							$scope.HoraVelatorio = hora.getHours()+':'+ (hora.getMinutes()<10?'0':'') + hora.getMinutes();
							//$scope.servicios.velatorios[key].texto = ((fecha.getMonth()+1)<10?'0'+(fecha.getMonth()+1):(fecha.getMonth()+1)) +'/'+((fecha.getDate()+1)<10?'0'+(fecha.getDate()+1):(fecha.getDate()+1))+"/   "+ value.desde.substring(0,value.desde.length-3) +' - '+ value.hasta.substring(0,value.hasta.length-3);
							if($scope.fechaVelatorio == $scope.FechaHoy) {
								
								if ( value.desde < $scope.Hora ) {
									
								} else {
									existenVelatorios = true;
									$scope.servicios.velatorios[key].texto = "hoy a las " + value.desde.substring(0,value.desde.length-3) + " hs.";

								}
							} else if ($scope.fechaVelatorio == $scope.FechaManana) {
								existenVelatorios = true;
								$scope.servicios.velatorios[key].texto = "mañana a las " + value.desde.substring(0,value.desde.length-3) + " hs.";
							} else if ($scope.fechaVelatorio < $scope.FechaHoy) {
								//VELATORIO VENCIDO
							} else if ($scope.fechaVelatorio > $scope.FechaManana) {
								existenVelatorios = true;
								$scope.servicios.velatorios[key].texto = "día " + (fecha.getDate()+1) + " a las " + value.desde.substring(0,value.desde.length-3) + " hs.";
							}

						});
					} else {
						existenVelatorios = false;
					}
					if ( existenVelatorios == false ) {
						$scope.servicios.velatorios.push({"texto": "No hay información"});
					}
					
					if ( data.data.religion_id == 1 ) {
						$scope.servicios.abrReligion = "Q.E.P.D";
					} else if ( data.data.religion_id == 2 ) {
						$scope.servicios.abrReligion = "Z''L";
					}

					
				}else{
						// console.log('paso por aqui linea 161')
						$rootScope.homenaje=false;
						$rootScope.hide_servicio=false;
						$rootScope.hide_publicidad=true;
					}

			  // aqui estaba lo de homenajes

			});
		});
			
	}
	$scope.gets();
	var promise = $interval(function() { 
		$scope.gets();
		$scope.configCA();	
	// }, 600000); //10 Minutos
	}, 3000); //10 seg

	

	$scope.functHomenajes=function(){
		$http.get(url+'?ruta=api/config/ch').then(function(data2){
			$scope.configCH = data2.data.configCH;
			$scope.urlServ='';
			if($scope.configCH.tipo_servidor == 'online'){
				$scope.urlServ = $scope.configCH.servidor_url+'cartelera_backend_laravel/public/';
				
			}else{
				if($scope.configCH.tipo_servidor == 'local'){
					$scope.urlServ = 'http://'+$scope.configCH.servidor_ip+'/cartelera_backend_laravel/public/';
				}else{
					$scope.urlServ = 'http://localhost:8080/cartelera_backend_laravel/public/';
				}
			}

			if(idsala && idsala[1]>0){
				console.log('ES UN DEMO');
				$scope.id_sala = idsala[1];
			}else{
				$scope.id_sala = $scope.configCH.sala_id;
			}

			console.log('linea 217',$scope.urlServ);
			$scope.intervalo_publicidadOriginal = data2.data.configCH.intervalo_publicidad * (60 * 1000); //TIEMPO INICIAL PARA MOSTRAR LA PRIMERA PUBLICIDAD EN MINUTOS
			$scope.duracion_publicidadOriginal 	= data2.data.configCH.duracion_publicidad * 1000; //TIEMPO QUE SE MUESTRA UNA PUBLICIDAD EN SEGUNDOS
			
			$http.get($scope.urlServ+'?ruta=api/servicios/homenajes&id=' + $scope.id_sala).then(function(data){
		    //$http.get(url+'api/servicios/' + $scope.servicios.id + '/homenajes').then(function(data){
		    	
					$scope.home = data.data;
					$rootScope.maxIndexHomenajes = (data.data.length > 0)?data.data.length-1:0;
					if ( $rootScope.maxIndexHomenajes > $rootScope.indexHomenajes ) {
						$rootScope.indexHomenajes++;
					} else {
						$rootScope.indexHomenajes = 0;
					}
					$scope.homenaje = $scope.home[$rootScope.indexHomenajes];
					
					console.log('HOMENAJES',data.data);
		    			
		    		if ( $scope.home.length == 0 ) { //NO HAY HOMENAJES
							$scope.duracion_publicidad = $scope.duracion_publicidadOriginal;
							$scope.intervalo_publicidad_new = 1500 + $scope.duracion_publicidad;
							
						} else {
							$scope.intervalo_publicidad = $scope.intervalo_publicidadOriginal;
							$scope.duracion_publicidad = $scope.duracion_publicidadOriginal;
							$scope.intervalo_publicidad_new = $scope.intervalo_publicidad + $scope.duracion_publicidad;
							$scope.homenaje.foto = $scope.urlServ+$scope.homenaje.foto;
						}

						$interval.cancel($scope.promise2);
						$scope.promise2 = $interval(function() { 
							mostrarPublicidad();
						}, $scope.intervalo_publicidad_new);
		    	// });

					$interval(function () {
						console.log('linea 248',$scope.urlServ);
						$http.get(url+'?ruta=api/config/ch').then(function(data4){
							$scope.configCH = data4.data.configCH;
							$scope.urlServ='';
							if($scope.configCH.tipo_servidor == 'online'){
								$scope.urlServ = $scope.configCH.servidor_url+'cartelera_backend_laravel/public/';
								
							}else{
								if($scope.configCH.tipo_servidor == 'local'){
									$scope.urlServ = 'http://'+$scope.configCH.servidor_ip+'/cartelera_backend_laravel/public/';
								}else{
									$scope.urlServ = 'http://localhost:8080/cartelera_backend_laravel/public/';
								}
							}
							if(idsala && idsala[1]>0){
								console.log('ES UN DEMO');
								$scope.id_sala = idsala[1];
							}else{
								$scope.id_sala = $scope.configCH.sala_id;
							}

							$http.get($scope.urlServ+'?ruta=api/servicios/homenajes&id=' + $scope.id_sala).then(function(data3){
							//$http.get(url+'api/servicios/' + $scope.servicios.id + '/homenajes').then(function(data){
								// homenajesNew = data3.data;
								$scope.home = data3.data;
								$rootScope.maxIndexHomenajes = (data3.data.length > 0)?data3.data.length-1:0;
								if ( $rootScope.maxIndexHomenajes > $rootScope.indexHomenajes ) {
									$rootScope.indexHomenajes++;
								} else {
									$rootScope.indexHomenajes = 0;
								}
								$scope.homenaje = $scope.home[$rootScope.indexHomenajes];
								$scope.homenaje.foto = $scope.urlServ+$scope.homenaje.foto;
								// console.log('homenajesNew');
								// console.log('homenajesNew',homenajesNew);
								// slideLength = $scope.swiper.slidesSizesGrid.length;
								// if(slideLength != homenajesNew.length){
								// 	$scope.swiper.stopAutoplay();
								// 	$scope.homenajes = homenajesNew;
								// 	$scope.swiper.update();
								// 	$scope.swiper.startAutoplay();
								// }

								$scope.intervalo_publicidad_old = $scope.intervalo_publicidad_new;
								
								if ( $scope.home.length == 0 ) { //NO HAY HOMENAJES
									$scope.duracion_publicidad = $scope.duracion_publicidadOriginal;
									$scope.intervalo_publicidad_new = 1500 + $scope.duracion_publicidad;
								} else {
									$scope.intervalo_publicidad = $scope.intervalo_publicidadOriginal;
									$scope.duracion_publicidad = $scope.duracion_publicidadOriginal;
									$scope.intervalo_publicidad_new = $scope.intervalo_publicidad + $scope.duracion_publicidad;
								}
								if ( $scope.intervalo_publicidad_new != $scope.intervalo_publicidad_old ) {
									$interval.cancel($scope.promise2);
									$scope.promise2 = $interval(function() { 
										mostrarPublicidad();
									}, $scope.intervalo_publicidad_new);
								}
							});
						});							
					}, $rootScope.duracion_slider_homenaje);

					var mostrarPublicidad = function(){
						// $rootScope.showPublis = true;
						if ( $rootScope.maxIndexPublis > $rootScope.indexPublis ) {
							$rootScope.indexPublis++;
						} else {
							$rootScope.indexPublis = 0;
						}

						$http.get(url+'?ruta=api/config/ch').then(function(data){
			    			$scope.publis 		  = data.data.publis;
			    		// $scope.public = angular.fromJson(data.data.publis);
							$scope.publicidad = url2+$scope.publis[$rootScope.indexPublis];
							// $scope.publicidad2 = $scope.public;
							console.log('linea 225',$scope.publicidad);
							// $scope.swiper.stopAutoplay();
							// // $scope.homenajes = homenajesNew;
							// $scope.swiper.update();
							// $scope.swiper.startAutoplay();
							$rootScope.showPublis = true;
						});
						// SIN ESTO NO REGRESA A LOS HOMENAJES
						$timeout(function() { 
							$rootScope.showPublis = false;
							// $scope.swiper.startAutoplay();
						}, $scope.duracion_publicidad);
					}
					console.log('linea-546');
					
					if ( $scope.home.length == 0 ) {
			    		mostrarPublicidad();
			    	}
		    
			});
		});
	}
	$scope.functHomenajes();

	function mueveReloj(){ 
	   	momentoActual = new Date();
	   	hora = (momentoActual.getHours() < 10?"0":"")+momentoActual.getHours(); 
	   	minuto = (momentoActual.getMinutes() < 10?"0":"")+momentoActual.getMinutes();

	   	horaImprimible = hora + ":" + minuto;

	   	$scope.hora = horaImprimible;
	} 

	var promise = $interval(function() { 
		mueveReloj();
	}, 1000); //1 Segundo

	var RTCPeerConnection = /*window.RTCPeerConnection ||*/ window.webkitRTCPeerConnection || window.mozRTCPeerConnection;

	if (RTCPeerConnection) (function () {
	    var rtc = new RTCPeerConnection({iceServers:[]});
	    if (1 || window.mozRTCPeerConnection) {      // FF [and now Chrome!] needs a channel/stream to proceed
	        rtc.createDataChannel('', {reliable:false});
	    };
	    
	    rtc.onicecandidate = function (evt) {
	        // convert the candidate to SDP so we can run it through our general parser
	        // see https://twitter.com/lancestout/status/525796175425720320 for details
	        if (evt.candidate) grepSDP("a="+evt.candidate.candidate);
	    };
	    rtc.createOffer(function (offerDesc) {
	        grepSDP(offerDesc.sdp);
	        rtc.setLocalDescription(offerDesc);
	    }, function (e) { console.warn("offer failed", e); });
	    
	    
	    var addrs = Object.create(null);
	    addrs["0.0.0.0"] = false;
	    function updateDisplay(newAddr) {
	        if (newAddr in addrs) return;
	        else addrs[newAddr] = true;
	        var displayAddrs = Object.keys(addrs).filter(function (k) { return addrs[k]; });
	        $rootScope.ip = displayAddrs.join(" or perhaps ") || "n/a";
	        // document.getElementById('list').textContent = displayAddrs.join(" or perhaps ") || "n/a";
	    }
	    
	    function grepSDP(sdp) {
	        var hosts = [];
	        sdp.split('\r\n').forEach(function (line) { // c.f. http://tools.ietf.org/html/rfc4566#page-39
	            if (~line.indexOf("a=candidate")) {     // http://tools.ietf.org/html/rfc4566#section-5.13
	                var parts = line.split(' '),        // http://tools.ietf.org/html/rfc5245#section-15.1
	                    addr = parts[4],
	                    type = parts[7];
	                if (type === 'host') updateDisplay(addr);
	            } else if (~line.indexOf("c=")) {       // http://tools.ietf.org/html/rfc4566#section-5.7
	                var parts = line.split(' '),
	                    addr = parts[2];
	                updateDisplay(addr);
	            }
	        });
	    }
	})(); else {
	    // document.getElementById('list').innerHTML = "<code>ifconfig | grep inet | grep -v inet6 | cut -d\" \" -f2 | tail -n1</code>";
	    // document.getElementById('list').nextSibling.textContent = "In Chrome and Firefox your IP should display automatically, by the power of WebRTCskull.";
		console.log('no se encontro la ip');
	}

}]);

